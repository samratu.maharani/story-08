from django.urls import path
from .views import book, data

app_name = 'appstory8'

urlpatterns = [
    path('', book, name = 'bookList'),
    path('data/', data, name = 'books_data')
]