from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from .views import book, data
import unittest


# Create your tests here.
class UnitTest(TestCase):
    def test_url_valid(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_index(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_url_invalid(self):
        response = Client().get('/notexist/')
        self.assertEqual(response.status_code, 404)

    def test_header(self):
        request = HttpRequest()
        response = book(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Cari Buku", html_response)


