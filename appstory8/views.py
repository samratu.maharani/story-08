from django.shortcuts import render
from django.http import JsonResponse
import json
import requests


# Create your views here.
def book(request):
    response = {}
    return render(request, 'index.html', response) 

def data(request):
    try:
        q = request.GET['q']

    except:
        q = "nofound"

    json_read = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + q).json()

    return JsonResponse(json_read)
